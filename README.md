# README #

Тестовое задание на знание SQL.

### Условия ###

1. Скачать учебную базу mySQL с официального сайта ([https://dev.mysql.com/doc/index-other.html](Link URL));
1. Написать запрос, который выбирает всех актёров, разбивает в процентном соотношении, для каждого актёра, в каких жанрах фильмов он снимался.

### Примечание ###

* учебную базу данных можно взять из данного репозитория (папка dump);
* решение задачи находится в папке src.