﻿SELECT 
	actor.actor_id AS actor_ident,
	CONCAT(actor.first_name, ' ', actor.last_name) AS full_name,
    category.name as film_category,
    TRUNCATE(count(actor.actor_id)/ti.allCount*100,2) AS percents
FROM actor
INNER JOIN film_actor ON actor.actor_id=film_actor.actor_id 
LEFT JOIN film ON film_actor.film_id=film.film_id 
LEFT JOIN film_category ON film_category.film_id=film.film_id 
LEFT JOIN category ON film_category.category_id=category.category_id
LEFT JOIN 
(
    SELECT film_actor.actor_id as acc, COUNT(film_actor.actor_id) as allCount 
    FROM film_actor 
    GROUP BY film_actor.actor_id
) ti 
ON actor.actor_id = ti.acc
GROUP BY actor.actor_id, category.category_id;